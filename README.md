# Default starter for Gridsome

This WAS the project you get when you run `gridsome create new-project`. It's since been expanded with 

1. Netlify CMS
2. SASS
3. ESLint
4. Prettify

### 1. Install Gridsome CLI tool if you don't have

`npm install --global @gridsome/cli`

### 2. Create a Gridsome project

1. `gridsome create my-gridsome-site` to install default starter
2. `cd my-gridsome-site` to open the folder
3. `gridsome develop` to start a local dev server at `http://localhost:8080`
4. Happy coding 🎉🙌



[![Netlify Status](https://api.netlify.com/api/v1/badges/bda46400-0347-4924-9be5-47681edd7908/deploy-status)](https://app.netlify.com/sites/hello-gridsome/deploys)